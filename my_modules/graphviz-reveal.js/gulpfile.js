const babel = require('@rollup/plugin-babel').default
const commonjs = require('@rollup/plugin-commonjs')
const resolve = require('@rollup/plugin-node-resolve').default
const {rollup} = require('rollup')
const {terser} = require('rollup-plugin-terser')
const webpack = require('webpack-stream');
const { src, dest } = require('gulp');

const babelConfig = {
    babelHelpers: 'bundled',
    ignore: ['node_modules'],
    compact: false,
    extensions: ['.js', '.html'],
    plugins: [
        'transform-html-import-to-string'
    ],
    presets: [[
        '@babel/preset-env',
        {
            corejs: 3,
            useBuiltIns: 'usage',
            modules: false
        }
    ]]
};

exports.default = function() {
    return Promise.all([
     { name: 'RevealGraphviz', input: './src/plugin.js', output: './dist/graphviz-reveal' }]
     .map( plugin => {
    rollup({
        input: plugin.input,
        plugins: [
            webpack(),
            resolve(),
            commonjs(),
            babel({
                ...babelConfig,
                ignore: [/node_modules\/(?!(highlight\.js|marked)\/).*/],
            }),
            terser()
        ]
    }).then( bundle => {
        bundle.write({
            file: plugin.output + '.esm.js',
            name: plugin.name,
            format: 'es'
        })

        bundle.write({
            file: plugin.output + '.js',
            name: plugin.name,
            format: 'umd'
        })
    });
} ), 
src('./node_modules/@hpcc-js/wasm/dist/expatlib.wasm').pipe(dest('./dist'),
src('./node_modules/@hpcc-js/wasm/dist/graphvizlib.wasm').pipe(dest('./dist')
)));
}