---
title: "Доска"
block: "Инструменты"
order: 1
customTheme : "presentation"

---

### Ня

---

### ДОСКА ДЛЯ ФОРМУЛ

<div id="left">
<textarea id="board" class="editor" for="board_math">  

* $S→aXbX|aZ$ 
* $X→aY|bY|\lambda$
* $Y→X|cc$
* $Z→ZX$

</textarea>
</div>
<div id="right" class="board_math">

</div>

---

### ДОСКА ДЛЯ ГРАФИКОВ


<div id="left">
<textarea id="board" class="editor_graphviz" for="board_graph">  


</textarea>
</div>
<div id="right" class="board_graph">

</div>