---
title: "Основы научной работы для школьников или как написать свою первую статью на примере задач ИИ.  "
block: "Разное"
order: 0

---


## Основы научной работы для школьников или как написать свою первую статью на примере задач ИИ.    

## НИУ МЭИ

### Москва, 2021


---

### Вводная

<div id="left">

Привет!

Меня зовут Сириус и ко мне можно на ты ^_^

А так я:  <!-- .element: class="fragment" -->
* Ивлиев Сергей Андреевич  <!-- .element: class="fragment" -->
* кандидат технических наук  <!-- .element: class="fragment" -->
* старший преподаватель на кафедре Прикладной Математике и Искусственного Интеллекта  <!-- .element: class="fragment" -->

Меня можно найти:  <!-- .element: class="fragment" -->
* vk: https://vk.com/siriusfreak  <!-- .element: class="fragment" -->
* telegram: https://t.me/siriusfreak  <!-- .element: class="fragment" -->
* inst: https://www.instagram.com/siriusfreak/  <!-- .element: class="fragment" -->
* fb: https://www.facebook.com/siriusfrkru  <!-- .element: class="fragment" -->
* и мой сайтик https://siriusfrk.ru  <!-- .element: class="fragment" -->
* А тут можно оставить отзыв: https://docs.google.com/forms/d/1bZnCdon5KON8UgSqWTv7Bv5hlnuGAnMNEC8Nta2JFtE/edit <!-- .element: class="fragment" -->

</div>

<div id="right">

<img src="/images/tdp_26_02_2021/sirius.jpg" height="600px"/>

</div>

---

### Цель на сегодня

Рассказать тебе, как сделать научную работу, которая будет иметь вес и может быть опубликована. <!-- .element: class="fragment" -->

Ожидаемый итог: <!-- .element: class="fragment" -->
* ты сможешь принять участие в научных конференциях <!-- .element: class="fragment" -->
* ты представишь свой проект на соревнованиях <!-- .element: class="fragment" -->
* ты сможешь сделать свои первые публикации <!-- .element: class="fragment" -->

---

### Что нас ждёт

1. Важные слова о том что такое Наука <!-- .element: class="fragment" -->
2. Рассказ о том, чем полезны научные публикации <!-- .element: class="fragment" -->
3. Рассмотрение разных задач, связанных с ИИ <!-- .element: class="fragment" -->
4. Формулировка пайплайна для подготовки публикации с нуля <!-- .element: class="fragment" -->
5. Подготовка и аналитика <!-- .element: class="fragment" -->
6. Создание модели, обучение и оценка качества <!-- .element: class="fragment" -->
7. Советы по оформлению работы <!-- .element: class="fragment" -->
8. Список мест (не полный!) куда можно податься с результатами <!-- .element: class="fragment" -->

---

### Что нас ждёт

Примеры

<iframe width="1063" height="802" src="https://www.youtube.com/embed/tlOIHko8ySg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Что нас ждёт

Практика!

<img src="/images/school_science_12_03_2021/anaconda.png" />

---

### Чего нас не ждёт

1. Решение каких-либо проектных задач <!-- .element: class="fragment" -->
2. Разработка законченных приложений <!-- .element: class="fragment" -->
3. Глубокое изучение каких-либо инструментов <!-- .element: class="fragment" -->

---

### Что же такое наука?

Википедия говорит, что нау́ка — область человеческой деятельности, направленная на выработку и систематизацию объективных знаний о действительности. Эта деятельность осуществляется путём сбора фактов, их регулярного обновления, систематизации и критического анализа. На этой основе выполняется синтез новых знаний или обобщения, которые описывают наблюдаемые природные или общественные явления и указывают на причинно-следственные связи, что позволяет осуществить прогнозирование. Те гипотезы, которые описывают совокупность наблюдаемых фактов и не опровергаются экспериментами, признаются законами природы или общества.

---

### Что же такое наука?

Наука:

1. Вырабатывает и систематизирует знания <!-- .element: class="fragment" -->
2. Собирает, обновляет, систематизирует и анализирует факты <!-- .element: class="fragment" -->
3. Синтезирует, обобщает, описывает причинно-следственные связи, прогнозирует <!-- .element: class="fragment" -->
4. Формулирует гипотезы <!-- .element: class="fragment" -->


---

### Так как в это въехать?

На самом деле всё просто. Так как есть:

1. Научный метод. Набор теорий, методов, законов, инструментов и вообще хороших практик. <!-- .element: class="fragment" -->
2. Критерии научности. Позволяет отделить науку от чего-то-наукообразного. <!-- .element: class="fragment" -->
3. Критерии научной новизны. Они позволяют понять полезно ли будет то, что вы напишите. <!-- .element: class="fragment" -->
4. Наставники. Которые, возможно, будут предлагать что-то скучное, но потом вам это понравится (или нет).  <!-- .element: class="fragment" -->

---

### Научный метод

1. Знание - результат познания, который можно обосновать и практически проверить  <!-- .element: class="fragment" -->
2. Теория - система знаний, в отношении какого-либо явления <!-- .element: class="fragment" -->
3. Гипотеза - предположение, описывающее совокупность наблюдаемых фактов и не опровергающееся экспериментами <!-- .element: class="fragment" -->
3. Научный закон - проверенная гипотеза, сформулированная вербально или математически и принятая как текущее объяснение наблюдаемых явлений <!-- .element: class="fragment" -->

---

### Научный метод

4. Моделирование - создание моделей с переносом полученных из них знаний на оригинал. <!-- .element: class="fragment" -->
5. Наблюдения - целенаправленное рассмотрение предметов действительности с фиксированием в описании <!-- .element: class="fragment" -->
6. Измерение - определение количественных характеристик объекта <!-- .element: class="fragment" -->
7. Эксперименты - специально сформулированные протоколы наблюдений, которые позволяют подтверждать причинно-следственные связи, проводить измерения и т.д. <!-- .element: class="fragment" -->

---

### Критерии научности

Он же критерий Поппера, он же критерий фальсифицируемости.

**Научная теория не может быть принципиально неопровержимой** <!-- .element: class="fragment" -->

или <!-- .element: class="fragment" -->

**Теория считается научной, если можно придумать эксперимент, который её опровергнет** <!-- .element: class="fragment" -->

---

### Научная новизна

* научные факты изучены в новых условиях: в другом месте, времени, регионе; <!-- .element: class="fragment" --> 
* существующий метод исследования применен к новой задаче или проблеме; <!-- .element: class="fragment" -->
* известная научная теория применена к новым объектам или предметам; <!-- .element: class="fragment" -->
* изучен новый аспект существующего знания; <!-- .element: class="fragment" -->
* получены новые результаты в опытах; <!-- .element: class="fragment" -->
* из практики выведен новый научный подход; <!-- .element: class="fragment" -->
* сформулирована новая для науки модель объекта/предмета/действия; <!-- .element: class="fragment" -->
* выведена новая научная гипотеза.<!-- .element: class="fragment" -->

---

### Наставники

Важно найти того, кто готов тебе помогать в развитии, тратить на тебя своё время. 

Может казаться, особенно в школе, что это нечто запредельное и этого достойны только лучшие. <!-- .element: class="fragment" -->

Но по факту: есть огромное количество людей, которые готовы помочь вам. И вот я тут ^_^ <!-- .element: class="fragment" -->

---

### Что мы будем делать сегодня?

Будем применять существующие структуры нейронных сетей и методов кластеризации на данных электроретинографии. 

---

### Зачем нужны публикации?

1. Вы фиксируете результат своей работы (публикация = релиз) <!-- .element: class="fragment" -->
2. По публикациям вас можно оценить как специалиста <!-- .element: class="fragment" -->
3. Публикации позволяют другим изучать подходы к их проблеме, которые были рассмотрены ранее <!-- .element: class="fragment" -->
4. Если ваша публикация описывает что-то, что полезно другому, то вы можете найти много новых друзей ^_^ <!-- .element: class="fragment" -->

---

### А ещё

При подготовке публикации вы стремитесь сделать её результаты воспроизводимыми, то есть избавляетесь от случайных факторов, которые могли привести к неверным выводам. <!-- .element: class="fragment" -->

Вы учитесь грамотно формулировать свои мысли. <!-- .element: class="fragment" -->

Вы учитесь работать с источниками (но лучше бы с этого начинать). <!-- .element: class="fragment" -->

---

### Пример: физиогномика 

Метод определения типа личности человека, его душевных качеств и состояния здоровья, исходя из анализа внешних черт лица и его выражения.

Признана псевдонаукой, начиная с Нового Времени (но были те, кто и в XX веке её использовал). <!-- .element: class="fragment" -->

<img src="/images/school_science_12_03_2021/museo-di-storia-della-psichiatria-1.jpg" height="400px" class="fragment"/> 

---

### Пример: физиогномика 

В XXI веке Майкл Косински: 

* В 2017 году выпускает препринт, в котором показывает, что нейронные сети могут определять пристрастия человека по фотографии (https://psyarxiv.com/hv28a/) <!-- .element: class="fragment" -->
* В 2021 году публикует описание метода, который позволяет определить по фотографиям политические взгляды (https://www.nature.com/articles/s41598-020-79310-1) <!-- .element: class="fragment" -->

---

### Пример: физиогномика 

Структура модели:

<img src="/images/school_science_12_03_2021/41598_2020_79310_Fig1_HTML.webp" />

---

### Пример: физиогномика 

Результаты:

<img src="/images/school_science_12_03_2021/41598_2020_79310_Fig2_HTML.webp" />

---

### Пример: физиогномика 

Выводы:

1. Физиогномика от этого не становится наукой <!-- .element: class="fragment" -->
2. Жизненные предпочтения влияют на то, как мы выглядим <!-- .element: class="fragment" -->
3. Жизненные предпочтения влияют на то, какие фотографии мы делаем <!-- .element: class="fragment" -->

---

### Задачи ИИ

Искусственный Интеллект это не про повторение личности, сознания, человека или чего-то ещё.

Искусственный Интеллект это про решение задач, которые раньше считались прерогативой человеческого мышления: <!-- .element: class="fragment" -->

1. Принятие сложных решений <!-- .element: class="fragment" -->
2. Моделирование рассуждений <!-- .element: class="fragment" -->
3. Распознавание образов <!-- .element: class="fragment" -->
4. Аннотирование изображений <!-- .element: class="fragment" -->
5. Постановка диагнозов <!-- .element: class="fragment" -->

---

### Задачи ИИ

6. Предсказание курсов акций и валют <!-- .element: class="fragment" -->
7. Управление транспортными средствами <!-- .element: class="fragment" -->
8. Доказательство гипотез <!-- .element: class="fragment" -->
9. Написание программного кода <!-- .element: class="fragment" -->
10. Игра в компьютерные и обычные игры <!-- .element: class="fragment" -->
11. Уборка помещений и мойка окон <!-- .element: class="fragment" -->
12. Поиск и обобщение информации <!-- .element: class="fragment" -->
13. И многое, многое другое <!-- .element: class="fragment" -->

---

### Аннотирование изображений


<img src="/images/school_science_12_03_2021/cat_is_ready.png" />

---

### Управление транспортными средствами

<iframe width="1903" height="743" src="https://www.youtube.com/embed/tlThdr3O5Qo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Уборка помещений

<iframe width="1903" height="743" src="https://www.youtube.com/embed/tLt5rBfNucc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Игры

<iframe width="1903" height="743" src="https://www.youtube.com/embed/nbiVbd_CEIA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Пайплайн

Теперь рассмотрим как подготовить подобную работу.

Аналитика: <!-- .element: class="fragment" -->

1. Выбираем предметную область <!-- .element: class="fragment" -->
2. Ищем набор данных в рамках этой области <!-- .element: class="fragment" -->
3. Ищем публикации, в рамках которых этот набор данных рассматривался <!-- .element: class="fragment" -->

Разработка: <!-- .element: class="fragment" -->

4. Строим свою модель для анализа <!-- .element: class="fragment" -->
5. Выбираем критерии качества <!-- .element: class="fragment" -->
6. Оцениваем качество работы модели <!-- .element: class="fragment" -->

---

### Пайплайн

Оформление: <!-- .element: class="fragment" -->

7. Изучаем требования к оформлению <!-- .element: class="fragment" -->
8. Пишем введение о важности и научной новизне работы <!-- .element: class="fragment" -->
9. Делаем обзор существующих методов <!-- .element: class="fragment" -->
10. Рассказываем что сделали сами <!-- .element: class="fragment" -->
11. Показываем сравнение с другими методами  <!-- .element: class="fragment" -->
12. Пишем заключение о планах на будущее <!-- .element: class="fragment" -->
13. Переводим на английский <!-- .element: class="fragment" -->

---

### Выбор предметной области

Проблемы:
1. Всегда хочется крутого <!-- .element: class="fragment" -->
2. Недостаток опыта <!-- .element: class="fragment" -->
3. Непонимание практической пользы <!-- .element: class="fragment" -->
4. Сложно сформулировать задачу <!-- .element: class="fragment" -->

Решения:
1. Выбросить идею и обсудить <!-- .element: class="fragment" -->
2. Выбрать активно развивающиеся области <!-- .element: class="fragment" -->
3. Обратиться к тому, кто может дать тему и курировать <!-- .element: class="fragment" -->

---

### Поиск набора данных

Проблемы:
1. Данных мало <!-- .element: class="fragment" -->
2. Данных много <!-- .element: class="fragment" -->
3. Данных нет <!-- .element: class="fragment" -->
4. Данные противоречивы <!-- .element: class="fragment" -->
5. Данные неразмечены <!-- .element: class="fragment" -->

---

### Поиск набора данных

Решения:
1. Скрепинг сайтов (https://habr.com/ru/post/280238/) <!-- .element: class="fragment" -->
2. Самостоятельных сбор данных  <!-- .element: class="fragment" -->
3. Использование готовых наборов https://github.com/awesomedata/awesome-public-datasets (а ещё в некоторых случаях есть решения) <!-- .element: class="fragment" -->
4. Фильтрация данных <!-- .element: class="fragment" -->
5. Аугментация данных данные (https://habr.com/ru/company/smartengines/blog/264677/) <!-- .element: class="fragment" -->


---

###  Поиск публикаций

Проблема: найти и скачать статью или другую работу, где рассматривался наш набор данных (или похожий). <!-- .element: class="fragment" -->

1. Поиск статей: https://scholar.google.com/ Можно задать поиск по названию набора данных и найти соответствующую статью. <!-- .element: class="fragment" -->

<img src="/images/school_science_12_03_2021/google_scholar.png" class=
"fragment" />

---

### Скачивание публикаций

2. https://vk.com/sci_hub

<img src="/images/school_science_12_03_2021/sci_hub.png" />

---

### После аналитики

1. Есть набор данных или метод его сбора и подготовки <!-- .element: class="fragment" -->
2. Есть публикации по выбранному набору данных и (или) по смежным темам <!-- .element: class="fragment" -->

Можно написать введение, обосновать значимость работы и сделать обзор источников. <!-- .element: class="fragment" -->

---

### Практика (моя)

Моя область: заболевания глаза.

Мой набор данных: записи ЭРГ.

<img src="/images/school_science_12_03_2021/img-dxxeO4.png">

---

### Практика (моя)

Проблемы:
1. Данные хранятся в аппарате, который выгружает только PDF с картинками по одному за раз <!-- .element: class="fragment" -->
2. Данные не размечены, все записи в амбарных книгах <!-- .element: class="fragment" -->
3. Данные зашумлены, есть записи не только от людей, но ещё от кроликов <!-- .element: class="fragment" -->

<img src="/images/school_science_12_03_2021/photo_2019-01-28_10-49-55.jpg" class="fragment">

---

### Практика (моя)

Решение:

1. Был проведён реверс-инженеринг аппарата для снятия ЭРГ и извлечены из него данные (http://dx.doi.org/10.15827/0236-235X.127.512-517) <!-- .element: class="fragment" -->
2. Разработана система для разметки данных врачом-экспертом <!-- .element: class="fragment" -->

<img src="/images/school_science_12_03_2021/diss.png" class="fragment">

---

### Практика (моя)

А публикаций было мало по теме, поэтому рассматривал смежные области анализа ЭКГ и ЭЭГ. <!-- .element: class="fragment" -->

Поэтому я мог понять какие модели можно использовать и каких результатов стоит ожидать. <!-- .element: class="fragment" -->

---

### Практика (моя)

Итог:

1. $>40000$ ЭРГ разных видов в базе <!-- .element: class="fragment" -->
2. Вышла на контакт группа из УРФУ, которая занимается подобным и которая тоже не могла вытащить данные. <!-- .element: class="fragment" -->
3. Было определено, что применение каких-либо методов анализа к ЭРГ уже заключает в себе научную новизну в виду малого количества работ. <!-- .element: class="fragment" -->
4. Поскольку много данных остаётся неразмеченными, я решил провести кластеризацию данных и посмотреть какие есть варианты. <!-- .element: class="fragment" -->

---

### Разработка. 

Мы будем решать задачу с использованием популярных фреймворков и языка Python.

Просто потому что это быстро и удобно.  <!-- .element: class="fragment" -->

Лучше всего использовать пакет Anaconda, в который уже включены все инструменты. <!-- .element: class="fragment" -->

Альтернативы -- онлайн сервисы: <!-- .element: class="fragment" -->

1. Google Colab (https://colab.research.google.com/) <!-- .element: class="fragment" -->
2. Yandex.DataSphere (https://cloud.yandex.com/services/datasphere) <!-- .element: class="fragment" -->
3. DataLore (https://datalore.jetbrains.com/) <!-- .element: class="fragment" -->

---

### Модели

Под моделью в машинном обучении подразумевается некоторая математическая модель. 

Данная модель реализует некоторую функцию, которая по входным данным делает некоторый прогноз выходных. <!-- .element: class="fragment" -->

Модель может обучаться на примерах соответствия между входными и выходными данными. <!-- .element: class="fragment" -->

Нейронная сеть - пример такой модели. <!-- .element: class="fragment" -->

---

### Модели

1. Общий список: https://tproger.ru/translations/top-machine-learning-algorithms/ <!-- .element: class="fragment" -->
2. Нейросетевые: https://tproger.ru/translations/neural-network-zoo-1/ https://tproger.ru/translations/neural-network-zoo-2/ <!-- .element: class="fragment" -->

Есть модели, которые лучше подходят к одним данным и хуже к другим. Есть модели заточенные не определённые задачи. <!-- .element: class="fragment" -->

---

### Модели

Изучить и попробовать можно здесь: https://ru.coursera.org/specializations/machine-learning-data-analysis <!-- .element: class="fragment" -->

Как получить материальную помощь (то есть бесплатный доступ к курсу), описано здесь https://wiki.appmat.ru/index.php/Coursera) <!-- .element: class="fragment" -->

---

### Критерии качества

Критерии качества позволяют понять насколько модель хорошо выполняет те задачи, которые на неё возложены.

Критерии могут быть как математическими, так и практическими. <!-- .element: class="fragment" -->

Математические критерии задаются разными формулами, описывающими соотношения величин. <!-- .element: class="fragment" -->

Практические критерии показывают можно ли применять модель "в бою", к примеру автомодерировать контент, формировать подборки новостей и так далее. <!-- .element: class="fragment" -->

Также качество работы модели может оцениваться экспертом. <!-- .element: class="fragment" -->

---

### Критерии качества

Матрица ошибок (для двухклассовой классификации):

<img src="/images/school_science_12_03_2021/confusion_matrix.png" class=
"fragment">

---

### Критерии качества

* Точность – сколько всего результатов было предсказано верно; <!-- .element: class="fragment" -->
* Доля ошибок - процент ошибок от общего числа примеров; <!-- .element: class="fragment" -->
* Полнота – сколько истинных результатов было предсказано верно; <!-- .element: class="fragment" -->
* F-мера, которая позволяет сравнить 2 модели, одновременно оценив полноту и точность. Здесь используется среднее гармоническое вместо среднего арифметического, сглаживая расчеты за счет исключения экстремальных значений. <!-- .element: class="fragment" -->

---

### Критерии качества

<img src="/images/school_science_12_03_2021/confusion_matrix_values.png" class="fragment">

---

### Практика

У нас есть ЭРГ и мы хотим их кластеризовать. Но одна ЭРГ - это 500 значений. Пространство кластеризации очень большое! <!-- .element: class="fragment" -->

Надо значит снизить размерность:

1. Можно выделить реперные точки (минимумы, максимумы) <!-- .element: class="fragment" -->
2. Можно сделать дискретное вейвлет преобразование (то самое, которое используется в jpg), но потерять данные <!-- .element: class="fragment" -->
3. Можно попробовать использовать автоэнкодер <!-- .element: class="fragment" -->
 
---

### Автоэнкодер

Это такая свёрточная глубинная нейронная сеть, которая позволяет сжать данные.

Для её обучения на вход и выход подаются одни и те же данные, а после обучения берутся значения из её центра. <!-- .element: class="fragment" -->

Эти значения и есть результат кодирования. <!-- .element: class="fragment" -->

<img src="/images/school_science_12_03_2021/avtojenkoder-e1536675599885.png" class="fragment" />

---

### Автоэнкодер

Но он же работает только с картинками!

Картинка -- это просто двумерная матрица, мы можем получить картинку из одномерного сигнала, если применим непрерывное вейвлет преобразование, которое позволяет выделить амплитудно-временные характеристики сигнала. <!-- .element: class="fragment" -->

То есть мы сперва стараемся подробнее рассмотреть наш сигнал, а потом сжимаем его до набор признаков, которые потом сможем кластеризовать. <!-- .element: class="fragment" -->

---

### Сигнал и его преобразование

<div id="left">

<img src="/images/school_science_12_03_2021/signal.png" />

 
</div>

<div id="right">

<img src="/images/school_science_12_03_2021/signal_cwt.png"/>

</div>

---

### Автоэнкодер

Обучение автоэнкодера.

<pre>
<code>
import keras
from keras import layers

input_img = keras.Input(shape=(84, 396, 1))

x = layers.Conv2D(16, (3, 3), activation='relu', padding='same')(input_img)
x = layers.MaxPooling2D((2, 2), padding='same')(x)
x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(x)
x = layers.MaxPooling2D((2, 2), padding='same')(x)
x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(x)
x = layers.MaxPooling2D((2, 2), padding='same')(x)
x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(x)
x = layers.MaxPooling2D((2, 2), padding='same')(x)
x = layers.Conv2D(4, (3, 3), activation='relu', padding='same')(x)
x = layers.MaxPooling2D((2, 2), padding='same')(x)
x = layers.Conv2D(4, (3, 3), activation='relu', padding='same')(x)
encoded = layers.MaxPooling2D((2, 2), padding='same')(x)

# at this point the representation is (2, 7, 4), i.e. 56 parameters


</code>
</pre>
---

### Автоэнкодер

<pre>
<code>

x = layers.Conv2D(4, (3, 3), activation='relu', padding='same')(encoded)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(4, (3, 3), activation='relu', padding='same')(encoded)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(encoded)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(encoded)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (5, 5), activation='relu', padding='same')(x)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (5, 5), activation='relu', padding='same')(x)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (6, 6), activation='relu', padding='same')(x)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (8, 8), activation='relu')(x)
x = layers.UpSampling2D((2, 2))(x)
x = layers.Conv2D(8, (9, 13), activation='relu')(x)
x = layers.UpSampling2D((2, 2))(x)
decoded = layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

autoencoder = keras.Model(input_img, decoded)
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

</code>
</pre>

---

### Автоэнкодер

Собственно часть автоэнкодера, которая выполняет кодирования.

<pre>
<code>

encoder = keras.Model(input_img, encoded)

encoded_imgs = encoder.predict(x_test)

</code>
</pre>

<img src="/images/school_science_12_03_2021/autoencoded.png" height="500px">

---

### Преобразование

Преобразуем все наши вейвлеты к сжатой форме.

<pre>
<code>
X_vec_n = []
for x in erg['wavelets']:
    X_vec_n.append(cut_wavelet(x))
    
X_vec_n_min = np.array(X_vec_n).min()
X_vec_n_max = np.array(X_vec_n).max()

X_vec_n = (np.reshape(X_vec_n, (len(X_vec_n), 84, 396, 1))  - X_vec_n_min) / (X_vec_n_max - X_vec_n_min) 
</code>
</pre>

---

### Кластеризация

После обучения мы можем проводить кластеризацию.

Как обычно, есть много методов. Поэтому для начала мы берём самый простой: метод K-средних. <!-- .element: class="fragment" -->

Он стремится разбить точки на группы, расстояние между точками в группе должно быть минимальным. <!-- .element: class="fragment" -->

---


### Кластеризация

<pre>
<code>

X_encoded = encoder.predict(X_vec_n)

X_encoded_reshaped = X_encoded.reshape(len(X_encoded), -1)

clustering = KMeans().fit(X_encoded_reshaped)

erg['clusters'] = clustering.labels_

</code>
</pre>

---

### Отрисовка кластеров

Код для отрисовки результатов, попавших в один кластер

<pre>
<code>

def plot_clusters(ind, count):
    l = len(erg.loc[erg['clusters'] == ind])
    vals = random.sample(range(0, l), count)
    for i in vals:
        plt.plot(erg.loc[erg['clusters'] == ind].iloc[i]['value'])
    plt.show()
</code>
</pre>
---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster0.svg" />

---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster1.svg" />

---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster2.svg" />

---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster3.svg" />

---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster4.svg" />

---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster5.svg" />


---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster6.svg" />


---

### Результат

Мы разбивали на 8 кластеров. Вот выборка сигналов, попавших в них.

<img src="/images/school_science_12_03_2021/cluster7.svg" />

---

### Вопросы

1. Как ещё можно улучшить качество? <!-- .element: class="fragment" -->
1. Помогли ли нам наши преобразованяи улучшить качество работы метода, чем если бы мы кластеризировали сырые данные? <!-- .element: class="fragment" -->
2. Есть ли другие методы, которыми мы могли обработать наши сигналы? <!-- .element: class="fragment" -->
3. Какой бы результаты мы могли ожидать на других методах? <!-- .element: class="fragment" -->
4. Что мы вообще получили? <!-- .element: class="fragment" -->

---

### Оформляем работу

Требования к оформлению можно найти на сайтах журналов, конференций и конкурсов. Как правило все ожидают форматы Word или $\LaTeX{}$. <!-- .element: class="fragment" -->

Часто выкладывают шаблоны документов, в которые надо просто вставить свои данные. <!-- .element: class="fragment" -->

---

### Оформляем работу

Типовая структра работы:

1. Аннотация - краткий (150-300 слов) обзор содержания статьи <!-- .element: class="fragment" -->
2. Ключевые слова - (5-6 слов или словосочетаний) тэги, по которым можно найти вашу статью и понять что в ней <!-- .element: class="fragment" -->
3. Введение - описание важности работы <!-- .element: class="fragment" -->
4. Обзор существующих методов - рассказываете про те методы, что рассмотрели (иногда это включают в ведение) <!-- .element: class="fragment" -->

---

### Оформляем работу

5. Рассказ о собственном методе. <!-- .element: class="fragment" -->
6. Сравнение метода с ранее описанными или заключение эксперта. <!-- .element: class="fragment" -->
7. Заключение по итогам работы. <!-- .element: class="fragment" -->
8. Сведения о грантах <!-- .element: class="fragment" -->
9. Список литературы <!-- .element: class="fragment" -->

В список литературы включаются те статьи, которые вы рассмотрели в процессе написания работы. По тексту работы должны быть ссылки на них. <!-- .element: class="fragment" -->

---

### Планы на будущее

Важно рассказать что ещё можно будет сделать в работе, какие методы можно будет применить, назвать недостатки текущей работы. 

---

### Перевод на английский

Если пока с английским плохо (что плохо само по себе), то можно воспользоваться следующей комбинацией: <!-- .element: class="fragment" -->

1. Переводчик типа https://translate.google.com/ <!-- .element: class="fragment" -->
2. Система, позволяющая исправлять грамматику и стиллистику https://www.grammarly.com/ <!-- .element: class="fragment" -->
3. Обратный перевод полученной статьи на русский для валидации корректности изложения <!-- .element: class="fragment" -->

---

### $\LaTeX{}$

Это система, которая позволяет "программировать" содержимое документа. 

В ней много команд, которые позволяют управлять оформлением документа. В том числе удобно работать с математическими формулами. <!-- .element: class="fragment" -->

Есть модули для автоматического формирования библиографических сведений. <!-- .element: class="fragment" -->

---

### Подаём результаты

1. ОЦ "Сириус" в Сочи: https://sochisirius.ru/obuchenie/nauka <!-- .element: class="fragment" -->
2. Шаг в Будущее: https://olymp.bmstu.ru/ru/front <!-- .element: class="fragment" -->
3. https://1502.mskobr.ru/edu-news/4462 (старое) <!-- .element: class="fragment" -->
